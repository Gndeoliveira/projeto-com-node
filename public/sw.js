var CACHE_NAME = 'staticV2';

this.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME).then(function(cache) {
            return cache.addAll([
                './',
                //
                 '../css/styles.css',
                //
                 '../js/scripts.js',
                //
                '../assets/img/cabin.png',
                '../assets/img/cake.png',
                '../assets/img/circus.png',
                '../assets/img/game.png',
                '../assets/img/safe.png',
                '../assets/img/submarine.png',
                '../assets/img/avataaars.svg',
                '../assets/img/favicon.ico',
                '../assets/img/logo.png',
                //
                '../assets/mail/contact_me.js',
                '../assets/mail/jqBootstrapValidation.js',
                //
                '../manifest.json',
             ]);
        })
    );
});

self.addEventListener('activate', function activator(event) {
    event.waitUntil(
        caches.keys().then(function(keys) {
            return Promise.all(keys
                .filter(function(key) {
                    return key.indexOf(CACHE_NAME) !== 0;
                })
                .map(function(key) {
                    return caches.delete(key);
                })
            );
        })
    );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request).then(function(cachedResponse) {
            return cachedResponse || fetch(event.request);
        })
    );
});